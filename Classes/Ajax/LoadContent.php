<?php

namespace SGalinski\dfContentslide\Ajax;

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use SGalinski\ContentReplacer\Repository\TermRepository;
use SGalinski\ContentReplacer\Service\AbstractParserService;
use SGalinski\ContentReplacer\Service\CustomParserService;
use SGalinski\ContentReplacer\Service\SpanParserService;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Fluid\View\StandaloneView;

/**
 * Loads and renders the bodytext from a given record id with all required
 * transformations. Note that the class does not require a valid page and
 * always assumes zero as page id value.
 */
class LoadContent {
	/**
	 * @var string
	 */
	protected $content = '';

	/**
	 * @var \TYPO3\CMS\Fluid\View\StandaloneView
	 */
	protected $view;

	/**
	 * @var ObjectManager
	 */
	protected $objectManager;

	/**
	 * Controlling method
	 *
	 * @return void
	 */
	public function main() {
		$parameters = GeneralUtility::_GP('df_contentslide');
		$this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
		$this->view = $this->objectManager->get(StandaloneView::class);
		$this->initializeView();

		$content = $this->getContentByRecordId($parameters['id']);

		if (ExtensionManagementUtility::isLoaded('content_replacer')) {
			$spanParser = $this->getSpanParser();
			$content['bodytext'] = $this->parseAndReplace($spanParser, $content['bodytext']);

			$specialWrapCharacter = trim($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_content_replacer.']['specialParserCharacter']);
			if ($specialWrapCharacter !== '') {
				$customParser = $this->getCustomParser($specialWrapCharacter);
				$content['bodytext'] = $this->parseAndReplace($customParser, $content['bodytext']);
			}
		}

		$content['header_layout'] = 100;
		$content['tx_df_contentslide_contentslide'] = 0;

		$this->view->assign('data', $content);
		$this->content = $this->view->render();
	}

	/**
	 * Init the standalone view template
	 */
	protected function initializeView() {
		// initialize the Fluid header-partial
		$this->view->setTemplatePathAndFilename(
			GeneralUtility::getFileAbsFileName('EXT:df_contentslide/Resources/Private/Templates/AjaxContent.html')
		);
	}

	/**
	 * Prints the content
	 *
	 * @return void
	 */
	public function printContent() {
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT');
		header('Cache-Control: no-store, no-cache, must-revalidate');
		header('Cache-Control: post-check=0, pre-check=0', FALSE);
		header('Pragma: no-cache');

		echo '<div class="dfcontentslide-content">
			<div class="dfcontentslide-contentSub">' . $this->content . '</div>
		</div>';
	}

	/**
	 * Returns the bodytext of the record identified by the given uid
	 *
	 * @param int $id
	 * @return array
	 */
	protected function getContentByRecordId(int $id): array {
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tt_content');
		$row = $queryBuilder->select('*')
			->from('tt_content')
			->where(
				$queryBuilder->expr()->eq('uid', $id, \PDO::PARAM_INT)
			)
			->setMaxResults(1)
			->execute()->fetch();

		return $row;
	}

	/**
	 * Returns a span tag parser instance
	 *
	 * @return SpanParserService
	 * @throws \InvalidArgumentException
	 */
	protected function getSpanParser() {
		$spanParser = GeneralUtility::makeInstance(SpanParserService::class);
		$spanParser->setExtensionConfiguration($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_content_replacer.']);
		$spanParser->injectTermRepository(GeneralUtility::makeInstance(TermRepository::class));

		return $spanParser;
	}

	/**
	 * Returns a custom wrap character parser instance
	 *
	 * @param string $specialWrapCharacter
	 * @return CustomParserService
	 * @throws \InvalidArgumentException
	 */
	protected function getCustomParser($specialWrapCharacter) {
		$customParser = GeneralUtility::makeInstance(CustomParserService::class);
		$customParser->setExtensionConfiguration($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_content_replacer.']);
		$customParser->injectTermRepository(GeneralUtility::makeInstance(TermRepository::class));
		$customParser->setWrapCharacter($specialWrapCharacter);

		return $customParser;
	}

	/**
	 * Parses and replaces the content several times until the given parser cannot find
	 * any more occurrences or the maximum amount of possible passes is reached.
	 *
	 * @param AbstractParserService $parser
	 * @param string $content
	 * @return string
	 */
	protected function parseAndReplace(AbstractParserService $parser, $content) {
		$loopCounter = 0;
		while (TRUE) {
			if ($loopCounter++ > $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_content_replacer.']['amountOfPasses']) {
				break;
			}

			$occurences = $parser->parse($content);
			if (!count($occurences)) {
				break;
			}

			foreach ($occurences as $category => $terms) {
				$content = $parser->replaceByCategory($category, $terms, $content);
			}
		}

		return $content;
	}
}

/** @var $object LoadContent */
$object = GeneralUtility::makeInstance(LoadContent::class);
$object->main();
$object->printContent();

?>
