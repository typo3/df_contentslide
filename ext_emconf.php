<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "df_contentslide".
 *
 * Auto generated 28-06-2013 18:36
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF['df_contentslide'] = array(
	'title' => 'Dynamic Sliding of Content Elements with AJAX',
	'description' => 'Collapsing and expanding of content elements with dynamic or static loading.',
	'category' => 'fe',
	'shy' => 0,
	'version' => '7.1.1',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => 'tt_content',
	'clearcacheonload' => 1,
	'lockType' => '',
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan@sgalinski.de',
	'author_company' => '',
	'CGLcompliance' => '',
	'CGLcompliance_note' => '',
	'constraints' => array(
		'depends' => array(
			'php' => '7.0.0-7.3.99',
			'typo3' => '8.7.0-10.4.99',
		),
		'conflicts' => array(),
		'suggests' => array(),
	),
	'suggests' => array(),
);

?>
