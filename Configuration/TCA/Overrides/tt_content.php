<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

call_user_func(
	function ($extKey, $table) {
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns(
			$table,
			[
				'tx_' . $extKey . '_contentslide' => [
					'exclude' => TRUE,
					'label' => 'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_activate',
					'config' => [
						'type' => 'check',
						'default' => 0,
						'items' => [
							1 => [
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_activate'
							],
						],
					],
				],
				'tx_' . $extKey . '_options' => [
					'exclude' => TRUE,
					'label' => 'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_options',
					'config' => [
						'type' => 'check',
						'items' => [
							1 => [
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_options.withAjax'
							],
							2 => [
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_options.expanded'
							],
						],
					],
					'displayCond' => 'FIELD:tx_' . $extKey . '_contentslide:REQ:TRUE',
				],
				'tx_' . $extKey . '_layout' => [
					'exclude' => TRUE,
					'label' => 'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_layout',
					'config' => [
						'type' => 'select',
						'items' => [
							[
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_layout.layout1',
								'layout1'
							],
							[
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_layout.layout2',
								'layout2'
							],
							[
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_layout.layout3',
								'layout3'
							],
						],
					],
					'displayCond' => 'FIELD:tx_' . $extKey . '_contentslide:REQ:TRUE',
				],
				'tx_' . $extKey . '_animation' => [
					'exclude' => TRUE,
					'label' => 'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_animation',
					'config' => [
						'type' => 'select',
						'items' => [
							[
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_animation.slideDownwards',
								'slideDownwards'
							],
							[
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_animation.slideUpwards',
								'slideUpwards'
							],
							[
								'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_animation.fade',
								'fade'
							],
						],
					],
					'displayCond' => 'FIELD:tx_' . $extKey . '_contentslide:REQ:TRUE',
				],
				'tx_' . $extKey . '_header' => [
					'exclude' => TRUE,
					'label' => 'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_useCustomHeader',
					'config' => [
						'type' => 'input',
					],
					'displayCond' => 'FIELD:tx_' . $extKey . '_contentslide:REQ:TRUE',
				],
			]
		);

		$paletteLabel = 'LLL:EXT:' . $extKey . '/Resources/Private/Languages/locallang_db.xlf:' . $table . '.tx_' . $extKey . '_contentslide';
		$position = '--palette--;' . $paletteLabel . ';' . $extKey;

		$GLOBALS['TCA'][$table]['ctrl']['requestUpdate'] .= ', tx_' . $extKey . '_contentslide';

		$GLOBALS['TCA'][$table]['palettes'][$extKey] = [
			'showitem' => 'tx_' . $extKey . '_contentslide, tx_' . $extKey . '_options,' .
				'tx_' . $extKey . '_layout, tx_' . $extKey . '_animation',
			'canNotCollapse' => 1,
		];

		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes(
			$table, $position, '', 'after:layout'
		);
	}, 'df_contentslide', 'tt_content'
);
